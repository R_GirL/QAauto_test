from selenium.webdriver.common.by import By
import allure

from pages.base_page import BasePage


# класс для работы с главной страницей блога (/blog)
class MainPage(BasePage):
    POST_TITLE = '//h1[text()="{}"]'
    CREATE_POST_BUTTON = (By.ID, "new")
    FIRST_POST_TITLE = (By.TAG_NAME, "h1")
    NOTIFICATION = (By.ID, "alert_div")

    @allure.step("Клик по заголовку поста")
    def click_on_post_title(self, title):
        self.wait_until_clickable((By.XPATH, self.POST_TITLE.format(title))).click()

    @allure.step("Клик по кнопке создания поста")
    def click_create_post_button(self):
        self.wait_until_clickable(self.CREATE_POST_BUTTON).click()

    @allure.step("Проверка создания поста")
    def check_post_created_successfully_message(self):
        assert "Blog posted successfully!" in self.wait_until_visible(
            self.NOTIFICATION).text, \
            "Не отобразилось сообщение об успехе"

    @allure.step("Проверка что пост с данным заголовком существует")
    def check_post_exists(self, title):
        assert self.element_is_present((By.XPATH, self.POST_TITLE.format(title))), "Пост не опубликовался"

    @allure.step("Проверка невозможности создания поста")
    def check_impossibility_creating_post(self):
        assert not self.element_is_present(self.CREATE_POST_BUTTON)

    @allure.step("Проверка удалился ли пост")
    def check_post_is_deleted(self, title):
        assert "Your post was successfully deleted" in self.wait_until_visible(
            self.NOTIFICATION).text, "Не отобразилось сообщение об успехе"
        assert not self.element_is_present((By.XPATH, self.POST_TITLE.format(title))), "Удаление поста не произошло"
